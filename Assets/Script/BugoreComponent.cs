﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class BugoreComponent : MonoBehaviour {

	[ReadOnly] public int colourIndex;
	[ReadOnly] public List<GameObject> neighbours;

	public Tween tween;

	/// <summary>
	/// 	Recursivly updates the ref list with matches of the same colourIndex
	/// </summary>
	/// <param name="colourIndex">Colour index.</param>
	/// <param name="matchList">Match list.</param>
	public void GetMatchingNeightbours(int colourIndex, ref List<GameObject> matchList)
	{
		BugoreComponent bugComp;
		// Check neighbours for colour matches
		for (int i = 0; i < neighbours.Count; i++)
		{
			// Gnab the bugores component from the object
			bugComp = neighbours[i].GetComponent<BugoreComponent>();

			// If the neighbour colour matches the match search colour
			if (bugComp.colourIndex == colourIndex)
			{
				// And the bug isn't already in the list
				if (matchList.Contains(neighbours[i]) == false)
				{
					matchList.Add(neighbours[i]);
					bugComp.GetMatchingNeightbours(colourIndex, ref matchList);
				}
			}
		}
	}

	public void AddNeighbour(GameObject neighbour)
	{
		neighbours.Add(neighbour);
	}

	public void RemoveNeighbour(GameObject neighbour)
	{
		neighbours.Remove(neighbour);
	}

	void OnDestroy()
	{
		//Remove reference to this bug with it's Neighbours
       
		BugoreComponent bugComp;
		if (tween != null) tween.Kill(true);
		for (int i = 0; i < neighbours.Count; i++)
		{
			//Gnab the bugore component to clean up references
			bugComp = neighbours[i].GetComponent<BugoreComponent>();

			//Remove References to this guy
			bugComp.RemoveNeighbour(gameObject);
		}
	}
}