using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class BugoreBoardManager : BasicBoardManager {

	public List<GameObject> ColouredPrefabs;

	public delegate void OnAnimationCompleteAction();
	public static event OnAnimationCompleteAction OnAnimationComplete;

	private Sequence _boardTweenSequence;

	protected override GameObject getTile ()
	{
        // todo put this somewhere else in the game initialization
	    DOTween.useSafeMode = false;

		GameObject newTile = base.getTile ();

		//Decide it's colour
		int colourIndex = Random.Range(0, ColouredPrefabs.Count);

		//Set the bug's colour index
		BugoreComponent bugComp = newTile.GetComponent<BugoreComponent>();
		bugComp.colourIndex = colourIndex;

		//TODO: Pool here
		var tile = Instantiate(ColouredPrefabs[colourIndex]);
		newTile.name = tile.name;
		tile.transform.parent = newTile.transform;

		return newTile;
	}

	public void FillGaps()
	{
		for (int i = boardObjects.Count - 1; i >= 0; i--)
		{
			//Remove all null objects
			boardObjects[i].RemoveAll(item => item == null);
			if (boardObjects[i].Count == 0)
			{
				boardObjects[i] = null;
			}
		}
		boardObjects.RemoveAll(item => item == null);

		//UpdatePiecePosition();
	}

	#region Animations
	public void AnimateFillGaps()
	{
		_boardTweenSequence = DOTween.Sequence();
		BugoreComponent bugComp;

		for (int x = 0; x < boardObjects.Count; x++)
		{
			for (int y = 0; y < boardObjects[x].Count; y++)
			{
				bugComp = boardObjects[x][y].GetComponent<BugoreComponent>();

				bugComp.tween = boardObjects[x][y].transform.DOMove(new Vector2(-x, y), 0.5f).SetEase(Ease.InOutBack).SetEase(Ease.InBack);

				_boardTweenSequence.Insert(0, bugComp.tween);
			}
		}
		_boardTweenSequence.OnComplete(handleAnimationComplete);
	}

	public void AnimateBugsIn()
	{
		_boardTweenSequence = DOTween.Sequence();
		
		for (int x = 0; x < boardObjects.Count; x++)
		{
			for (int y = 0; y < boardObjects[x].Count; y++)
			{
				BugoreComponent bugComp = boardObjects[x][y].GetComponent<BugoreComponent>();
				bugComp.tween = bugComp.gameObject.transform.DOMove(new Vector2(-x, y), 1).SetEase(Ease.OutBack);
				_boardTweenSequence.Insert(0, bugComp.tween);
			}
		}
		
		_boardTweenSequence.OnComplete(handleAnimationComplete);
	}
	
	public void AnimateBugsOut()
	{
		_boardTweenSequence = DOTween.Sequence();
		
		for (int x = 0; x < boardObjects.Count; x++)
		{
			for (int y = 0; y < boardObjects[x].Count; y++)
			{
				BugoreComponent bugComp = boardObjects[x][y].GetComponent<BugoreComponent>();
				bugComp.tween = bugComp.gameObject.transform.DOMove(new Vector2(bugComp.gameObject.transform.position.x, 
				                                                                bugComp.gameObject.transform.position.y - 10), 0.75f).SetEase(Ease.InBack);
				_boardTweenSequence.Insert(0, bugComp.tween);
			}
		}
		
		_boardTweenSequence.OnComplete(handleAnimationComplete);
	}

	private void handleAnimationComplete()
	{
		OnAnimationComplete();
	}
	#endregion
}
