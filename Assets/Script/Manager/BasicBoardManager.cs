﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

public class BasicBoardManager : MonoBehaviour {

    // TODO - SEPERATE THE TILE INDEXES FROM THE ANIMATION SO THEY CAN BE MANAGED INDEPENDANTLY AND ALLOW FOR THINGS TO HAPPEN WHILE ANIMATIONS ARE TRIGGERED

	// Basic board variables
	[Header("Board Settings")]
	public int boardHeight = 1;
	public int boardWidth = 1;

	[Header("Tile Settings")]
	public int tileHeight = 1;
	public int tileWidth = 1;
	public GameObject tilePrefab;

	protected List<List<GameObject>> boardObjects { get; private set;}

	// Use this for allocation
	virtual protected void Awake() 
	{
		// Make sure dimensions are not 0
		if ((boardWidth + boardHeight) <= 1) 
			Debug.LogError("[BasicBoardManager::Awake] Board dimensions must be atleast 1x1.");
	}

	/// <summary>
	/// 	Builds a game board based on the requested number of lines
	/// </summary>
	/// <param name="NumberOfLines">Number of lines.</param>
	public void BuildBoard(int NumberOfLines)
	{
		//If the board doesn't exist yet. Make it.
		if (boardObjects == null) boardObjects = new List<List<GameObject>>();
		
		//Build empty board
		for (int x = 0; x < NumberOfLines; x++)
		{
			boardObjects.Insert(0, InternalAddLine());
		}
	}
    /// <summary>
    /// Called externally to add a new line to the board
    /// </summary>
    public void AddLine()
    {
        boardObjects.Insert(0, InternalAddLine());
    }

	/// <summary>
	/// 	Add a full Line of objects to the board
	/// </summary>
	/// <returns>The line.</returns>
	private List<GameObject> InternalAddLine()
	{
		List<GameObject> newLine = new List<GameObject>();
		
		for (int y = 0; y < boardHeight; y++)
		{
			// Add new tile to the new tile list.
			GameObject tile = getTile();
			
			//Add the new tile to the game
			tile.transform.position = new Vector2(0, y);
			tile.transform.parent = transform;
			
			newLine.Add(tile);
		}
		
		return newLine;
	}

	/// <summary>
	/// 	Get a tile for the board.
	/// 	Should be overridden
	/// </summary>
	/// <returns>The tile.</returns>
	protected virtual GameObject getTile()
	{
		//TODO: Grab from pool
		return Instantiate(tilePrefab);;
	}

	public void ResetBoard()
	{
		for (int x = boardObjects.Count - 1; x >= 0; x--)
		{
			for (int y = boardObjects[x].Count -1; y >= 0; y--)
			{
				RemoveTileFromBoardList(x, y);
			}
			
			boardObjects.RemoveAt(x);
		}
	}

    /// <summary>
    /// 	Remove object from specified x,y position. Leaves 'null' space
    /// </summary>
    /// <param name="x">The x coordinate.</param>
    /// <param name="y">The y coordinate.</param>
    private void RemoveTileFromBoardList(int x, int y)
    {
        Destroy(boardObjects[x][y]);
        boardObjects[x][y] = null;

    }
    /// <summary>
    /// External method to remove tiles from the board, used when making matchs
    /// Note: cant use coordinates unless they are sorted correctly or else the indexes changes which are based on the animations
    /// </summary>
    /// <param name="go"></param>
    public void RemoveTileFromBoardList(GameObject go)
	{
	    for (int x = boardObjects.Count - 1; x >= 0; x--)
	    {
	        int index = boardObjects[x].IndexOf(go);

	        if (index > -1)
	        {
	            Destroy(boardObjects[x][index]);
	            boardObjects[x][index] = null;
	        }
	    }
     
		//TODO: Put back in pool
		//Remove the tiles from the lists
		
	}
}
