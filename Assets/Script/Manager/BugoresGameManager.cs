﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof (BugoreBoardManager))]
public class BugoresGameManager : MonoBehaviour {

	private BugoreBoardManager _boardManager;
	private int _clickableLayer;

	//Game Details
	private int _currentLevel;

	//Tracking info
	private int _trackMatchCount;//How many matches we've done
	private int _trackHighestMatch;//The highest match count we've done
	private int _trackBugsSquished;

    // temp flag for tracking input state
    private bool inputEnabled = false;

	void Awake() {
		// Cache the clickable layer for ray casting.
		_clickableLayer = LayerMask.NameToLayer("Clickable");

		// Get reference to the Board Manager
		_boardManager = GetComponent<BugoreBoardManager>();
	}

	void Start()
	{
		//TODO: Make Levels be set by region map in future
		LevelManager.Level = 1;

		newBoard();
	}

	private void newBoard()
	{
		//Build Board ~> Draw Bugs ~> Animate In
		_boardManager.BuildBoard(7);
		_boardManager.AnimateBugsIn();
		
		//OnBoardReady
		BugoreBoardManager.OnAnimationComplete += HandleBoardReadyForInput;
	}

	public void AddLine()
	{
        // cant add a new line while stuff is tranistining for now
	    if (inputEnabled)
	    {
	        DisableTouch();
	        _boardManager.AddLine();
	        _boardManager.AnimateBugsIn();
	        BugoreBoardManager.OnAnimationComplete += HandleBoardReadyForInput;
	    }
	}

	private void HandleTouchStartEvent(Gesture gesture)
	{
		GameObject bugore = GetClickedBug(gesture);

		// Check if a bug was clicked
		if(bugore != null)
		{
			// Gather the whole list of matches
			BugoreComponent bugComp = bugore.GetComponent<BugoreComponent>();
			
			// Create the list and add our current match to it
			List<GameObject> matches = new List<GameObject>();
			matches.Add(bugore);
			
			// Check the neighbours for matches
			bugComp.GetMatchingNeightbours(bugComp.colourIndex, ref matches);
			
			Debug.Log("Touched on "+bugore.name+" and it's part of a "+matches.Count+" bug match.");
			
			if (matches.Count > 1)
			{
				//Increment the number of matches we've done
				_trackMatchCount += 1;
				//Check if this is the largest Match we've made
				_trackHighestMatch = (matches.Count>_trackHighestMatch)?matches.Count:_trackHighestMatch;
				//Increment our total bug squish count
				_trackBugsSquished += matches.Count;

				ScoreManager.Score += matches.Count * 2;

				//Stop Touches until end of animation
				DisableTouch();

				//Clean and update the board
				ClearMatches(matches);
				_boardManager.FillGaps();

				int lvl = (int)Mathf.Ceil(ScoreManager.Score / 50)+1;
				if ( LevelManager.Level < lvl)
				{
					//Level Changed
					LevelManager.Level = lvl;

					//Animate board out
					_boardManager.AnimateBugsOut();
					BugoreBoardManager.OnAnimationComplete += HandleAnimateBugsOutComplete;
					//When animation is done spawn in a new board
				}
				else
				{
					_boardManager.AnimateFillGaps();
					BugoreBoardManager.OnAnimationComplete += HandleBoardReadyForInput;
				}
			}
		}
	}

	/// <summary>
	/// 	Returns the clicked Bugore if one was clicked. Null is not.
	/// </summary>
	/// <returns>The clicked bug.</returns>
	private GameObject GetClickedBug(Gesture gesture)
	{
		GameObject bug = null;
		
		// Collect the ray cast data
		RaycastHit hitInfo = new RaycastHit();
		bool hit = Physics.Raycast(Camera.main.ScreenPointToRay(gesture.position), out hitInfo);
		
		// We hit a thing
		if (hit)
		{
			// Check if what we hit is on the clickable layer
			if (hitInfo.collider.transform.gameObject.layer == _clickableLayer)
			{
				bug = hitInfo.collider.transform.gameObject;
			}
		}
		
		return bug;
	}

	/// <summary>
	/// 	Removed the suppplied List of game objects from the board.
	/// </summary>
	/// <param name="matches">Matches.</param>
	private void ClearMatches(List<GameObject> matches)
	{
		for (int i = matches.Count - 1; i >= 0; i--)
		{
			//Remove the matched GameObject from the board's list
			_boardManager.RemoveTileFromBoardList(matches[i]);
		}
	}

	private void HandleBoardReadyForInput()
	{
		//Cleanup Events
		BugoreBoardManager.OnAnimationComplete -= HandleBoardReadyForInput;
		
		//Enable Touch and Start Push Timer
		EnableTouch();
	}

	private void HandleAnimateBugsOutComplete()
	{
		//Cleanup Events
		BugoreBoardManager.OnAnimationComplete -= HandleAnimateBugsOutComplete;

		//Rease Board
		_boardManager.ResetBoard();

		//Build Board ~> Draw Bugs ~> Animate In
		_boardManager.BuildBoard(7);
		_boardManager.AnimateBugsIn();
		
		//OnBoardReady
		BugoreBoardManager.OnAnimationComplete += HandleBoardReadyForInput;
	}
	
	/// <summary>
	/// 	Enables the touch events.
	/// </summary>
	private void EnableTouch()
	{
	    inputEnabled = true;
        EasyTouch.On_TouchStart += HandleTouchStartEvent;
	}
	
	/// <summary>
	/// 	Disables the touch events.
	/// </summary>
	private void DisableTouch()
	{
	    inputEnabled = false;
        EasyTouch.On_TouchStart -= HandleTouchStartEvent;
	}
}
