﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreManager : MonoBehaviour {

	public static int Score;

	private Text _scoreText;

	void Awake () {
		_scoreText = GetComponent<Text>();
		Score = 0;
	}

	void FixedUpdate () {
		_scoreText.text = "Score: " + Score;
	}
}
