﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LevelManager : MonoBehaviour {

	private static int _level;

	public static int Level {
		get {
			return _level;
		}
		set {
			_level = value;
			_levelText.text = "Level: "+_level;
		}
	}

	private static Text _levelText;

	void Awake () {
		_levelText = GetComponent<Text>();
	}

}
