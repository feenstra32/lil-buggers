﻿using UnityEngine;
using System.Collections;

public class BugoreFeeler : MonoBehaviour {

	public BugoreComponent owner;

	void OnTriggerEnter(Collider collider)
	{
		if (collider.gameObject.tag == "Bugore")
			owner.AddNeighbour(collider.gameObject);
	}

	void OnTriggerExit(Collider collider)
	{
		if (collider.gameObject.tag == "Bugore")
			owner.RemoveNeighbour(collider.gameObject);
	}
}
